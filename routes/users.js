var express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const User = mongoose.model('User');
const passwordUtils = require('../utils/genAndValidatePassword');
const jwtUtils = require('../utils/issueJwt');


/* POST register new user. */
router.post('/register', function(req, res, next){
  const saltHash = passwordUtils.genPassword(req.body.password);
   const salt = saltHash.salt;
   const hash = saltHash.hash;
   const newUser = new User({
       username: req.body.username,
       hash: hash,
       salt: salt
   });
   newUser.save().then((user) => {
       const jwt = jwtUtils.issueJWT(user);
       res.json({ success: true, message: "User created successfully"});
   })
   .catch(err => next(err));
});

/* POST log in an user. */
router.post('/login', function(req, res, next){
  User.findOne({ username: req.body.username })
  .then((user)=> {
      if (!user) {
        res.status(401).json( { success: false, message: 'Incorrect username.' });
      }
      const isValid = passwordUtils.validPassword(req.body.password, user.hash, user.salt);

      if(isValid){
          const tokenObject = jwtUtils.issueJWT(user);
          res.status(200).json( { success: true, token: tokenObject.token, expiresIn: tokenObject.expiresIn });
      } else {
          res.status(401).json( { success: false, message: 'You entered wrong credentials' });
      }
    }).catch((err)=>{
      res.status(500).json( { success: false, message: err.message });
    })
});


module.exports = router;
