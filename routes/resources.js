var express = require('express');
var router = express.Router();
const passport = require('passport');
const mongoose = require('mongoose');
const Resource = mongoose.model('Resource');

/* GET protected resources */
router.get('/protected/all', passport.authenticate('jwt', { session: false }), function(req, res, next) {
    Resource.find()
    .then((resources)=> {
        res.status(200).json( { success: true, resources: resources });
      }).catch((err)=>{
        res.status(500).json( { success: false, message: err.message });
      })
});

/* GET unprotected resources */
router.get('/unprotected/all', function(req, res, next) {
    Resource.find()
    .then((resources)=> {
        res.status(200).json( { success: true, resources: resources });
      }).catch((err)=>{
        res.status(500).json( { success: false, message: err.message });
      })
});

/* PUT create new resource */
router.put('/create', passport.authenticate('jwt', { session: false }), function(req, res, next) {
   const newResource = new Resource({
       name: req.body.name,
       type: req.body.type,
       url: req.body.url
   });
   newResource.save().then((resource) => {
    res.status(200).json( { success: true, message: "Resource created successfully", newResource: resource });
   })
   .catch(err => next(err));
});

/* DELETE a resource */
router.delete('/delete/:id', passport.authenticate('jwt', { session: false }), getResource, async function(req, res, next) {
    try {
        await res.resource.remove();
        res.status(200).json( { success: true, message: "Resource deleted successfully"});
      } catch (err) {
        res.status(400).json({ message: err.message});
      }
});

/* GET a resource */
router.get('/get/:id', passport.authenticate('jwt', { session: false }), getResource, async function(req, res, next) {
    res.status(200).json( { success: true, message: "Resource listed successfully", resource: res.resource });
});

/* UPDATE a resource */
router.patch('/update/:id', passport.authenticate('jwt', { session: false }), getResource, async function(req, res, next) {
    if(req.body.name != null){
        res.resource.name = req.body.name;
      }
    
      if(req.body.type != null){
        res.resource.type = req.body.type;
      }
      if(req.body.url != null){
        res.resource.url = req.body.url;
      }
      try {
        const updatedResource = await res.resource.save();
        res.status(200).json( { success: true, message: "Resource updated successfully", newResource: updatedResource });
      } catch (err) {
        res.status(400).json({ message: err.message});
      }
});

async function getResource(req, res, next){
    let resource;
    try {
        resource = await Resource.findById(req.params.id);
      if (resource == null){
        return res.status(404).json({ message: "Cannot find resource"} );
      }
    } catch (err) {
      return res.status(500).json({ message: err.message} );
    }
    res.resource = resource;
    next();
  }


module.exports = router;
