const mongoose = require('mongoose');

const ResourceSchema = new mongoose.Schema({
    name: String,
    type: String,
    url: String
});

mongoose.model('Resource', ResourceSchema);